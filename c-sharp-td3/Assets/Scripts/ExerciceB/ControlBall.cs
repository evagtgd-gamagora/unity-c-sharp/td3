﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBall : MonoBehaviour {

    Rigidbody rb;
    public float power = 1000;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if(rb != null)
        {
            Vector3 force = new Vector3(
            Input.GetAxis("Horizontal") * Time.deltaTime,
            0,
            Input.GetAxis("Vertical") * Time.deltaTime
            );

            rb.AddForce(power * force);
        }
	}
}
