﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwapOnEnter : MonoBehaviour {

    MeshRenderer meshRenderer;
    float startTime;
    Color startColor;

	// Use this for initialization
	void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer != null)
        {
            startColor = meshRenderer.material.GetColor("_Color");
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(meshRenderer != null)
        {
            if (Input.GetKey(KeyCode.KeypadEnter))
            {
                float delay = Time.time - startTime;

                Color newColor;
                if (delay < 1)
                    newColor = Color.white;
                else if (delay < 3)
                    newColor = Color.green;
                else if (delay < 5)
                    newColor = Color.yellow;
                else
                    newColor = Color.red;

                meshRenderer.material.SetColor("_Color", newColor);
            }
            else
            {
                meshRenderer.material.SetColor("_Color", startColor);
            }
        }
	}

    private void FixedUpdate()
    {
        if (meshRenderer != null)
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                startTime = Time.time;
                startColor = meshRenderer.material.GetColor("_Color");
            }
        }
    }
}
