﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerpOnEnter : MonoBehaviour {

    MeshRenderer meshRenderer;
    float startTime;

	// Use this for initialization
	void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if(meshRenderer != null)
        {
            if (Input.GetKey(KeyCode.KeypadEnter))
            {
                Color lerpedColor = Color.Lerp(Color.green, Color.red, Mathf.PingPong(Time.time - startTime, 1));
                meshRenderer.material.SetColor("_Color", lerpedColor);
            }
            else
            {
                meshRenderer.material.SetColor("_Color", Color.white);
            }
        }
	}

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            startTime = Time.time;
        }
    }
}
