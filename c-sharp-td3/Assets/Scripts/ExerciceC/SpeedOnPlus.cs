﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedOnPlus : MonoBehaviour {

    float startTime;
    public float speedConst = 10;

	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.KeypadPlus))
        {
            float speed = (Time.time - startTime) * speedConst;
            //Vector3 rotation = new Vector3(0, speed, 0);
            transform.RotateAround(Vector3.zero, Vector3.up, speed);
        }
	}

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            startTime = Time.time;
        }
    }
}
