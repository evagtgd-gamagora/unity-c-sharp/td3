﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollison : MonoBehaviour {

    int countCollision = 0;
    int maxCollisions = 3;

    MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }
    // Update is called once per frame
    void Update () {
		if(countCollision >= maxCollisions)
        {
            Destroy(gameObject);
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            ++countCollision;
            ColorSwap();
        }
    }

    void ColorSwap()
    {
        if(meshRenderer != null)
        {
            meshRenderer.material.SetColor("_Color", Random.ColorHSV());
        }
    }
}
