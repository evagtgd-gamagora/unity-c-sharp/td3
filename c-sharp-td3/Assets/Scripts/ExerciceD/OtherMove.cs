﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherMove : MonoBehaviour {

    float speedConst = 5;
    Vector3 rotatepoint;
	// Use this for initialization
	void Start () {
        rotatepoint = transform.position + (Vector3.zero - transform.position).normalized;
	}
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(rotatepoint, Vector3.up, speedConst);
	}
}
