﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyColliderOnC : MonoBehaviour {

    private Collider col;

	// Use this for initialization
	void Start () {
        col = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
        if (col != null && Input.GetKey(KeyCode.C))
            Destroy(col);
	}
}
