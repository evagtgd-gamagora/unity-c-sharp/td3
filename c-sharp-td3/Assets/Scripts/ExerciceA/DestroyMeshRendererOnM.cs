﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMeshRendererOnM : MonoBehaviour {

    private MeshRenderer meshRenderer;

	// Use this for initialization
	void Start () {
        meshRenderer = GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (meshRenderer != null && Input.GetKey(KeyCode.M))
            Destroy(meshRenderer);
    }
}
